const express = require('express');
const fs = require('fs');
const http = require('http');
const pug = require("pug");
const path = require('path');
const app = express();
const uuid= require('uuid');

var bodyParser = require('body-parser')


const port = 3000;
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, 'public')));

// parse application/x-www-form-urlencoded
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

const fileName = process.argv[2];

app.get('/cities', (req, res, next) => {
    fs.readFile(fileName, 'utf8', (err, data) => {
        const myData = JSON.parse(data);
        if (err) {
            next(err) // Pass errors to Express
        } else {
        res.render('template', {
            title: 'Lecture data.json',
            message: 'Contenu du fichier',
            data: myData
            })
        }
    });
});

app.post('/city', (req, res, next) => {
    const city = req.body.city;
    fs.readFile(fileName, 'utf8', (err, data) => {
        if (err) {
            next(err) // Pass errors to Express
        } else {
            const myData = JSON.parse(data);
            
            const existe = myData.cities.find(element => element.name == city);

            if (existe){
                console.log('erreur 500')
            }
            else {
                myData.cities.push({"id":uuid.v4(),"name":city});
            }

          fs.writeFile(fileName, JSON.stringify(myData), (err) => {
            if(err){
                console.error(err);
                next(err)
            }
            else {
                res.send(myData);
                console.log('File written successfully');
                }
            });
        }
    });
});

app.put('/city:id', (req, res, next) => {
    const city = req.body.city;
    const identifiant = req.body.city.id;
    fs.readFile(fileName, 'utf8', (err, data) => {
        if (err) {
            next(err) // Pass errors to Express
        } else {
            const myData = JSON.parse(data);

            const dataFiltre = myData.cities.filter(element => element.id == identifiant);
            //console.log(dataFiltre); Est sensé me renvoyer la liste filtrée mais me renvoie la ligne que je souhaite supprimer

            const dataUpd = dataFiltre.cities.push({"id":identifiant,"name":city});

            if (dataUpd){
                fs.writeFile(fileName, JSON.stringify(dataUpd), (err) => {
                    if(err){
                        console.error(err);
                        next(err)
                    }
                    else {
                        res.send(myData);
                        console.log('File written successfully');
                        }
                    });
            }
            else {
                console.log('erreur 500')
            }
        }
    });
});

app.delete('/city:id', (req, res, next) => {
    const city = req.body.city;
    fs.readFile(fileName, 'utf8', (err, data) => {
        if (err) {
            next(err) // Pass errors to Express
        } else {
            const myData = JSON.parse(data);

            const dataFiltre = myData.cities.filter(element => element.name == city);
            //console.log(dataFiltre); Est sensé me renvoyer la liste filtrée mais me renvoie la ligne que je souhaite supprimer


            if (dataFiltre){
                fs.writeFile(fileName, JSON.stringify(dataFiltre), (err) => {
                    if(err){
                        console.error(err);
                        next(err)
                    }
                    else {
                        res.send(myData);
                        console.log('File written successfully');
                        }
                    });
            }
            else {
                console.log('erreur 500')
            }
        }
    });
});

app.listen(port, () => console.log(`Serveur running on port ${port}`))